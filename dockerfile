FROM node:12-alpine

#ADDUSER
RUN mkdir /home/user-node
RUN adduser user-node -h /home/user-node -D
RUN chown user-node.user-node /home/user-node


#Create WORKDIR
RUN mkdir /opt/app
RUN chown user-node.user-node  /opt/app
USER user-node

#APP COPY
COPY . /opt/app
WORKDIR /opt/app
RUN npm install
RUN npm run test
EXPOSE 3000
CMD node index.js
